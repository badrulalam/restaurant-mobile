angular.module('starter.services', [])
    .constant('AccessLevels', {
        anon: 0,
        user: 1
    })
    .constant('Constants', {
        BaseURL: 'http://128.199.68.137:1337'
    })
    .factory('LocalService', function () {
        return {
            get: function (key) {
                return localStorage.getItem(key);
            },
            set: function (key, val) {
                return localStorage.setItem(key, val);
            },
            unset: function (key) {
                return localStorage.removeItem(key);
            }
        }
    })
    .factory('Auth', function ($http, LocalService, AccessLevels, Constants) {
        return {
            authorize: function (access) {
                if (access === AccessLevels.user) {
                    return this.isAuthenticated();
                } else {
                    return true;
                }
            },
            isAuthenticated: function () {
                return LocalService.get('auth_token');
            },
            login: function (credentials) {
                //104.236.249.183
                console.log("inside login auth: " + JSON.stringify(credentials).toString());
                var login = $http.post(Constants.BaseURL+'/auth/authenticate', credentials).
                    success(function (result) {
                        console.log("auth success");

                        LocalService.set('auth_token', JSON.stringify(result));
                    }).error(function (result) {
                        console.log("login error: " + result);
                    });
                //console.log("webroot: " + $rootScope.webRoot);
                return login;
            },
            logout: function () {
                // The backend doesn't care about logouts, delete the token and you're good to go.
                LocalService.unset('auth_token');
            },
            register: function (formData) {
                LocalService.unset('auth_token');
                var register = $http.post(Constants.BaseURL+'/auth/register', formData);
                register.success(function (result) {
                    LocalService.set('auth_token', JSON.stringify(result));
                });
                return register;
            }
        }
    })
    .factory('AuthInterceptor', function ($q, $injector) {
        var LocalService = $injector.get('LocalService');

        return {
            request: function (config) {
                var token;
                if (LocalService.get('auth_token')) {
                    token = angular.fromJson(LocalService.get('auth_token')).token;
                }
                if (token) {
                    config.headers.Authorization = 'Bearer ' + token;
                }
                return config;
            },
            responseError: function (response) {
                if (response.status === 401 || response.status === 403) {
                    LocalService.unset('auth_token');
                    $injector.get('$state').go('anon.login');
                }
                return $q.reject(response);
            }
        }
    })
    .config(function ($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    })


    .factory('Restaurant', function (Constants, $http) {
        return {
            all: function () {
                var restaurants = $http.get(Constants.BaseURL+'/restaurant').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return restaurants;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (resId) {
                var restaurant = $http.get(Constants.BaseURL+'/restaurant?id='+resId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return restaurant;
            }
        }
    })
    .factory('Menu', function (Constants, $http) {
        return {
            all: function () {
                var menus = $http.get(Constants.BaseURL+'/menu').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return menus;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (menuId) {
                var menu = $http.get(Constants.BaseURL+'/menu?id='+menuId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return menu;
            },
            getByRes: function (resId) {
                var menu = $http.get(Constants.BaseURL+'/menu?restaurant='+resId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return menu;
            }
        }
    })
    .factory('Cart', function (Constants, $http) {
        return {
            all: function () {
                var carts = $http.get(Constants.BaseURL+'/cart').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return carts;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (cartId) {
                var cart = $http.get(Constants.BaseURL+'/cart?id='+cartId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return cart;
            },
            getByUser: function (userId) {
                var carts = $http.get(Constants.BaseURL+'/cart?user_id='+userId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return carts;
            },
            getByStatus: function (isComplete) {
                var carts = $http.get(Constants.BaseURL+'/cart?is_complete='+isComplete).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return carts;
            }


        }
    })

    .factory('CartItem', function (Constants, $http) {
        return {
            all: function () {
                var cartitems = $http.get(Constants.BaseURL+'/cartitem').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return cartitems;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (cartitemId) {
                var cartitem = $http.get(Constants.BaseURL+'/cartitem?id='+cartitemId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return cartitem;
            },
            getByCart: function (cartId) {
                var cartitems = $http.get(Constants.BaseURL+'/cartitem?cart='+cartId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return cartitems;
            },
            remove: function (itemId) {
                var carts = $http.post(Constants.BaseURL+'/cartitem/destroy?id=' + item).success(function (response) {
                    console.log("successfull");
                },function(err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return carts;
            },
            update: function (itemId,params) {
                var carts = $http.post(Constants.BaseURL + '/cartitem/update/' + itemId + '?' + params).success(function (response) {
                    console.log(" cartitem updated");
                },function(err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return carts;
            }
        }
    })
    .factory('Order', function (Constants, $http) {
        return {
            all: function () {
                var orders = $http.get(Constants.BaseURL+'/order').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return orders;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (orderId) {
                var order = $http.get(Constants.BaseURL+'/order?id='+orderId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return order;
            }
        }
    })
    .factory('OrderItem', function (Constants, $http) {
        return {
            all: function () {
                var Orderitems = $http.get(Constants.BaseURL+'/orderitem').success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return Orderitems;
            },
            //remove: function (chat) {
            //    chats.splice(chats.indexOf(chat), 1);
            //},
            get: function (orderitemId) {
                var orderitem = $http.get(Constants.BaseURL+'/orderitem?id='+orderitemId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return orderitem;
            },
            getByOrder: function (orderId) {
                var orderitems = $http.get(Constants.BaseURL+'/orderitem?order='+orderId).success(function (resp) {
                    console.log('Success', resp);
                    // For JSON responses, resp.data contains the result
                    //$scope.restaurants = resp.data;
                }, function (err) {
                    console.error('ERR', err);
                    // err.status will contain the status code
                })
                return orderitems;
            }
        }
    })
/**
 * A simple example service that returns some data.
 */
    .factory('Friends', function () {
        // Might use a resource here that returns a JSON array

        // Some fake testing data
        var friends = [{
            id: 0,
            name: 'Ben Sparrow',
            notes: 'Enjoys drawing things',
            face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
        }, {
            id: 1,
            name: 'Max Lynx',
            notes: 'Odd obsession with everything',
            face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
        }, {
            id: 2,
            name: 'Andrew Jostlen',
            notes: 'Wears a sweet leather Jacket. I\'m a bit jealous',
            face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
        }, {
            id: 3,
            name: 'Adam Bradleyson',
            notes: 'I think he needs to buy a boat',
            face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
        }, {
            id: 4,
            name: 'Perry Governor',
            notes: 'Just the nicest guy',
            face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
        }];


        return {
            all: function () {
                return friends;
            },
            get: function (friendId) {
                // Simple index lookup
                return friends[friendId];
            }
        }
    })

    .service('LoginService', function ($q) {
        return {
            loginUser: function (name, pw) {
                var deferred = $q.defer();
                var promise = deferred.promise;

                if (name == 'user' && pw == 'secret') {
                    deferred.resolve('Welcome ' + name + '!');
                } else {
                    deferred.reject('Wrong credentials.');
                }
                promise.success = function (fn) {
                    promise.then(fn);
                    return promise;
                };
                promise.error = function (fn) {
                    promise.then(null, fn);
                    return promise;
                };
                return promise;
            }
        }
    });
