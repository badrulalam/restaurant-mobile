// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ngCordova', 'ui.bootstrap','ionic.rating', 'starter.controllers', 'starter.services'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)


            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            //live 128.199.68.137
            //$rootScope.webRoot = "http://localhost:1337";
            //$rootScope.webRoot = "http://128.199.68.137:1337";

        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
        $ionicConfigProvider.tabs.position('bottom');

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider
            .state('register', {
                url: '/register',
                templateUrl: 'templates/register.html',
                controller: 'RegisterController'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginController'
            })
            .state('menu', {
                url: '/menu/:id',
                templateUrl: 'templates/menu.html',
                controller: 'MenuCtrl'
            })
            .state('item', {
                url: '/item/:id',
                templateUrl: 'templates/item.html',
                controller: 'ItemCtrl'
            })
            .state('cart', {
                url: '/cart',
                templateUrl: 'templates/cart.html',
                controller: 'CartCtrl'
            })
            .state('cartitem', {
                url: '/cartitem/:id',
                templateUrl: 'templates/cartitem.html',
                controller: 'CartItemCtrl'
            })

            // setup an abstract state for the tabs directive
            .state('tab', {
                url: "/tab",
                abstract: true,
                templateUrl: "templates/tabs.html"
            })
            // Each tab has its own nav history stack:
            .state('tab.dash', {
                url: '/dash',
                views: {
                    'tab-dash': {
                        templateUrl: 'templates/tab-dash.html',
                        controller: 'DashCtrl'
                    }
                }
            })
            //.state('tab.order', {
            //    url: '/order',
            //    'tab-order': {
            //        templateUrl: 'templates/tab-order.html',
            //        controller: 'OrderCtrl'
            //    }
            //})
            .state('tab.restaurant', {
                url: '/restaurant',
                views: {
                    'tab-restaurant': {
                        templateUrl: 'templates/tab-restaurant.html',
                        controller: 'RestaurantCtrl'
                    }
                }
            })
            .state('tab.restaurant-details', {
                url: '/restaurant/restaurant-details:id',
                views: {
                    'tab-restaurant': {
                        templateUrl: 'templates/restaurant-details.html',
                        controller: 'RestaurantDetailsCtrl'
                    }
                }
            })
            // .state('menu', {
            //     url: 'menu',
            //     templateUrl: 'templates/menu1.html',
            //     controller: 'Menu1Ctrl'

            // })
            // .state('menu', {
            //     url: 'menu/:id',
            //     templateUrl: 'templates/menu.html',
            //     controller: 'MenuCtrl'

            // })
            .state('tab.chats', {
                url: '/chats',
                views: {
                    'tab-chats': {
                        templateUrl: 'templates/tab-chats.html',
                        controller: 'ChatsCtrl'
                    }
                }
            })
            .state('tab.chat-detail', {
                url: '/chats/:id',
                views: {
                    'tab-chats': {
                        templateUrl: 'templates/chat-detail.html',
                        controller: 'ChatDetailCtrl'
                    }
                }
            })

            .state('tab.orders', {
                url: '/orders',
                views: {
                    'tab-orders': {
                        templateUrl: 'templates/tab-orders.html',
                        controller: 'OrderCtrl'
                    }
                }
            })
            .state('tab.friend-detail', {
                url: '/friend/:friendId',
                views: {
                    'tab-friends': {
                        templateUrl: 'templates/friend-detail.html',
                        controller: 'FriendDetailCtrl'
                    }
                }
            })

            .state('tab.account', {
                url: '/account',
                views: {
                    'tab-account': {
                        templateUrl: 'templates/tab-account.html',
                        controller: 'AccountCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');

    });
