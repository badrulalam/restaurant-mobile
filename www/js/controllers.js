angular.module('starter.controllers', ['ionic.rating'])


    .controller('LoginController', function ($scope, $state, Auth, Constants, $cordovaToast) {

        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        };
        // $scope.email = "zahid@zahid.com";
        // $scope.password = "zahid";
        $scope.user = {};
        $scope.user.email = "robin@dexsa.org";
        $scope.user.password = "qwerty";
        //var cred = {email:$scope.email,password:$scope.password};
        // credential.user = $scope.user;
        /// credential.password = $scope.password;
        $scope.errors = [];
        //$scope.user = {};

        $scope.redirectToRegister = function () {
            $state.go('register');
        };

        //$scope.test = function(){
        //  console.log($scope.user.email);
        //};

        $scope.login = function (cred) {
            console.log("login pressed");
            $scope.errors = [];
            Auth.login(cred).success(function (result) {
                console.log("auth succeded");
                $state.go('tab.dash');
            }).error(function (err) {
                console.log(err);
                $scope.errors.push(err);

                $scope.showToast('Login failed, Please Try Again', 'short','center');
            });
        }
    })
    .controller('RegisterController', function ($scope, $state, Auth, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        // $scope.email = "qwerty@lol.com";
        // $scope.password = "qwerty";
        // $scope.confirmPassword = "qwerty";
        // $scope.test = "testing";


        // $scope.testMethod = (function(){
        //   alert($scope.test);
        // });

        //var regCred = {"email":$scope.email,"password":$scope.password,"confirmPassword":$scope.confirmPassword};

        $scope.errors = [];
        //$scope.user = {};
        $scope.register = function (regCred) {

            $scope.errors = [];
            Auth.register(regCred).success(function (result) {
                console.log("registration successful");
                $state.go('tab.dash');
            }).error(function (err) {
                $scope.errors.push(err);
                //console.log("error" + $scope.errors);
                $scope.showToast('Registration failed, Please Try again ', 'short','center');
            });
        }
    })
    .controller('DashCtrl', function ($scope, $http, Constants, Restaurant, CartItem, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        //http://104.236.249.183:1337/restaurant
        $scope.cartitemno = 0;

        CartItem.all().success(function (resp) {
            resp.forEach(function (item) {
                console.log(item.item_qty);
                $scope.cartitemno += Number(item.item_qty);
            })
            console.log($scope.cartitemno);
        });


        Restaurant.all().success(function (result) {
            //console.log(JSON.stringify(result));
            //console.log("restaurent from service succeded");
            $scope.restaurants = result;
        }).error(function (err) {
            console.log(err);
            //$scope.errors.push(err);
        });

        //$http.get(Constants.BaseURL+'/restaurant').then(function (resp) {
        //    console.log('Success', resp);
        //    // For JSON responses, resp.data contains the result
        //    $scope.restaurants = resp.data;
        //}, function (err) {
        //    console.error('ERR', err);
        //    // err.status will contain the status code
        //})
    })
    .controller('RestaurantCtrl', function ($scope, $http, Constants,Restaurant, CartItem, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }

        $scope.restaurants = {};
        //http://104.236.249.183:1337/restaurant
        $scope.cartitemno = 0;

        CartItem.all().success(function (resp) {
            resp.forEach(function (item) {
                console.log(item.item_qty);
                $scope.cartitemno += Number(item.item_qty);
            })
            console.log($scope.cartitemno);
        });


        Restaurant.all().success(function (result) {
            //console.log(JSON.stringify(result));
            //console.log("restaurent from service succeded");
            $scope.restaurants = result;
        }).error(function (err) {
            console.log(err);
            //$scope.errors.push(err);
        });

        //$http.get(Constants.BaseURL+'/restaurant').then(function (resp) {
        //    console.log('Success', resp);
        //    // For JSON responses, resp.data contains the result
        //    $scope.restaurants = resp.data;
        //}, function (err) {
        //    console.error('ERR', err);
        //    // err.status will contain the status code
        //})
    })
    .controller('RestaurantDetailsCtrl', function ($scope, $http, $stateParams, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }


        $http.get(Constants.BaseURL + '/item?restaurant=' + $stateParams.id).then(function (resp) {
            console.log('Success', resp);
            // For JSON responses, resp.data contains the result
            $scope.items = resp.data;
        }, function (err) {
            console.error('ERR', err);
            // err.status will contain the status code
        })
    })
    .controller('ChatsCtrl', function ($scope, $http, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        //http://104.236.249.183:1337/restaurant
        $http.get(Constants.BaseURL + '/restaurant').then(function (resp) {
            console.log('Success', resp);
            // For JSON responses, resp.data contains the result
            $scope.restaurants = resp.data;
        }, function (err) {
            console.error('ERR', err);
            // err.status will contain the status code
        })
    })
    .controller('MenuCtrl', function ($scope, $http, $stateParams, Auth, Constants, Restaurant, CartItem, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }




        $scope.mycartitem = {};
        $scope.mycartitem.no = 0;

        CartItem.all().success(function (resp) {
            resp.forEach(function (item) {
                console.log(item.item_qty);
                $scope.mycartitem.no += Number(item.item_qty);
            })
            console.log($scope.mycartitem.no);
        });

        $scope.isDetails = false;

        $scope.slideImages = [
            {url: 'http://placehold.it/300x100&text=Image+One!', description: 'Image 00'},
            {url: 'http://placehold.it/300x100&text=Image+Two!', description: 'Image 01'},
            {url: 'http://placehold.it/300x100&text=Image+Three!', description: 'Image 02'},
            {url: 'http://placehold.it/300x100&text=Image+Four!', description: 'Image 03'},
            {url: 'http://placehold.it/300x100&text=Image+Five!', description: 'Image 04'}
        ];
        $scope.res = {};

        Restaurant.get($stateParams.id).success(function (result) {
            $scope.res = result;
        }).error(function (err) {
            console.log(err);
        });

        console.log("Menu Controller");
        $http.get(Constants.BaseURL + '/menu?restaurant=' + $stateParams.id).then(function (resp) {
            console.log('Success', resp);
            // For JSON responses, resp.data contains the result
            // $scope.res = resp.data[0].restaurant;
            $scope.menus = resp.data;
        }, function (err) {
            console.error('ERR', err);
            // err.status will contain the status code
        });
        $scope.addToCart = function (menu) {

            $scope.rate = 3;
            $scope.max = 5;

            $scope.usr = JSON.parse(Auth.isAuthenticated());

            $http.get(Constants.BaseURL + '/cart?user_id=' + $scope.usr.user.id).then(function (resp) {
                console.log('Success', resp);
                $scope.cart = {};
                $scope.cart.user_id = $scope.usr.user.id;
                $scope.cart.is_complete = false;
                $scope.cart.total = 0;
                if (resp.data.length === 0) {
                    $http.post(Constants.BaseURL + '/cart/create?', $scope.cart).success(function (response) {
                        $scope.cartitem = {};
                        $scope.cartitem.item_id = menu.id;
                        $scope.cartitem.item_name = menu.name;
                        $scope.cartitem.item_qty = 1;
                        $scope.cartitem.item_price = menu.price;
                        $scope.cartitem.total_price = Number($scope.cartitem.item_qty) * Number($scope.cartitem.item_price);
                        $http.post(Constants.BaseURL + '/cartitem/create?cart=' + response.id, $scope.cartitem).success(function (response) {
                            console.log(" first cartitem added");
                        });
                        console.log("cart added");
                    });
                }
                else {
                    $http.get(Constants.BaseURL + '/cartitem?item_id=' + menu.id).success(function (response) {
                        if (response.length === 0) {
                            console.log("inside if");
                            $scope.cartitem = {};
                            $scope.cartitem.item_id = menu.id;
                            $scope.cartitem.item_name = menu.name;
                            $scope.cartitem.item_qty = 1;
                            $scope.cartitem.item_price = menu.price;
                            $scope.cartitem.total_price = Number($scope.cartitem.item_qty) * Number($scope.cartitem.item_price);

                            $http.post(Constants.BaseURL + '/cartitem/create?cart=' + response.id, $scope.cartitem).success(function (response) {
                                console.log(" new cartitem added");

                                $scope.showToast('Item Added to Cart', 'short','center');
                            });
                        }
                        else {
                            console.log("inside else");
                            $scope.cartitem = {};
                            $scope.cartitem.item_id = menu.id;
                            $scope.cartitem.item_name = menu.name;
                            $scope.cartitem.item_qty = Number(response[0].item_qty) + 1;
                            $scope.cartitem.item_price = menu.price;
                            $scope.cartitem.total_price = Number($scope.cartitem.item_qty) * Number($scope.cartitem.item_price);

                            console.log($scope.cartitem.total_price);
                            $http.post(Constants.BaseURL + '/cartitem/update/' + response[0].id + '?item_qty=' + $scope.cartitem.item_qty + '&total_price=' + $scope.cartitem.total_price).success(function (response) {
                                console.log(" cartitem updated");
                                $scope.showToast('Item Added to Cart', 'short','center');
                            });
                        }

                        console.log("cart is not empty");
                    });
                }
                // For JSON responses, resp.data contains the result
            }, function (err) {
                console.error('ERR', err);
                // err.status will contain the status code
            });
        }
    })
    .controller('ItemCtrl', function ($scope, $http, $stateParams, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        console.log("Menu Controller");
        $http.get(Constants.BaseURL + '/item?menu=' + $stateParams.id).then(function (resp) {
            console.log('Success', resp);
            // For JSON responses, resp.data contains the result
            $scope.items = resp.data;
        }, function (err) {
            console.error('ERR', err);
            // err.status will contain the status code
        });
    })
    .controller('CartCtrl', function ($scope, $http, $stateParams, Constants, Cart, CartItem, Menu, Order, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        $scope.cartitems = [];
        $scope.totalprice = 0;
        CartItem.all().success(function (resp) {
            resp.forEach(function (item) {
                Menu.get(item.item_id).success(function (response) {
                    item.menu_name = response.name;
                    item.res_name = response.restaurant.name;
                    $scope.cartitems.push(item);
                    console.log(response.name + " " + response.restaurant.name + "\n");
                    $scope.totalprice += Number(item.total_price);
                })
            });
            console.log($scope.cartitemno);
        });

        function updateCart(){
            $scope.cartitems = [];
            CartItem.all().success(function (resp) {
                resp.forEach(function (item) {
                    Menu.get(item.item_id).success(function (response) {
                        item.menu_name = response.name;
                        item.res_name = response.restaurant.name;
                        $scope.cartitems.push(item);
                        console.log(response.name + " " + response.restaurant.name + "\n");
                        $scope.totalprice += Number(item.total_price);
                    })
                });
                console.log($scope.cartitemno);
            });
        }

        $scope.minusItem = function(gotItem){

            var itemqty = Number(gotItem.item_qty);
            if(itemqty>1){
                var itemqtyNum = itemqty -1;
                var itemtotalprice = Number(gotItem.total_price)- Number(gotItem.item_price);
                var itemParams = "item_qty="+itemqtyNum+"&total_price="+itemtotalprice;
                CartItem.update(gotItem.id,itemParams).success(function(response){
                    console.log("one item removed");
                    $scope.showToast('You have changed item quantity', 'short','center');
                    updateCart();
                })
            }
            else
            {
                CartItem.remove(gotItem.id).success(function(response){
                    console.log(response);
                    $scope.showToast('One item deleted', 'short','center');
                    updateCart();
                })
            }


        };
        $scope.addItem = function(gotItem){
            var itemqty = Number(gotItem.item_qty);
            var itemqtyNum = itemqty +1;
            var itemtotalprice = Number(gotItem.total_price) + Number(gotItem.item_price);
            var itemParams = "item_qty="+itemqtyNum+"&total_price="+itemtotalprice;
            CartItem.update(gotItem.id,itemParams).success(function(response){
                console.log("one item added");
                $scope.showToast('You have increased item quantity', 'short','center');
                updateCart();
            })
        };
        $scope.deleteItem = function(itemId){
            CartItem.remove(itemId).success(function(response){
                console.log(response);
                $scope.showToast('One item deleted', 'short','center');
                updateCart();
            })
        };
        $scope.clearAll = function(){
            $scope.cartitems.forEach(function(item){
                CartItem.remove(item.id).success(function(response){
                    console.log(response);
                    $scope.showToast('Cart Cleared', 'short','center');
                    updateCart();
                })
            })
        };
        $scope.order={};
        $scope.order.number = 0;
        $scope.order.price = 0;
        $scope.checkOut = function(cartId){
            $scope.cartitems.forEach(function(eachItem){
                $scope.order.number += Number(eachItem.item_qty);
                $scope.order.price += Number(eachItem.item_price);
            })
            $http.post(Constants.BaseURL + '/order/create?', $scope.order).success(function (response) {
                console.log(" order added");

            });
            $scope.showToast('You have placed and order', 'short','center');
        };

        //console.log("Menu Controller");
        //$http.get(Constants.BaseURL+'/item?menu=' + $stateParams.id ).then(function(resp) {
        //    console.log('Success', resp);
        //    // For JSON responses, resp.data contains the result
        //    $scope.items = resp.data;
        //}, function(err) {
        //    console.error('ERR', err);
        //    // err.status will contain the status code
        //});
    })
    .controller('CartItemCtrl', function ($scope, $http, $stateParams, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        //console.log("Menu Controller");
        //$http.get(Constants.BaseURL+'/item?menu=' + $stateParams.id ).then(function(resp) {
        //    console.log('Success', resp);
        //    // For JSON responses, resp.data contains the result
        //    $scope.items = resp.data;
        //}, function(err) {
        //    console.error('ERR', err);
        //    // err.status will contain the status code
        //});
    })
    .controller('OrderCtrl', function ($scope, $http, $stateParams, Constants, Order, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        console.log("order controller");
        $scope.showToast('You have no previous order', 'short','center');
        $scope.orders = [];
        Order.all().success(function(response){
            $scope.orders = response;
        })
    })
    .controller('ChatsCtrl', function ($scope, $http, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }


        //http://104.236.249.183:1337/restaurant
        $http.get(Constants.BaseURL + '/restaurant').then(function (resp) {
            console.log('Success', resp);
            // For JSON responses, resp.data contains the result
            $scope.restaurants = resp.data;
        }, function (err) {
            console.error('ERR', err);
            // err.status will contain the status code
        })
    })

    .controller('ChatDetailCtrl', function ($scope, $stateParams, $http, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }

        //104.236.249.183
        $http.get(Constants.BaseURL + '/item?restaurant=' + $stateParams.id).then(function (resp) {
            console.log('Success', resp);
            // For JSON responses, resp.data contains the result
            $scope.items = resp.data;
        }, function (err) {
            console.error('ERR', err);
            // err.status will contain the status code
        })
    })
    .controller('FriendsCtrl', function ($scope, Friends, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        $scope.friends = Friends.all();
    })

    .controller('FriendDetailCtrl', function ($scope, $stateParams, Friends, Constants, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        $scope.friend = Friends.get($stateParams.friendId);
    })

    .controller('AccountCtrl', function ($scope, Auth, Constants, $state, $cordovaToast) {
        $scope.showToast = function(message, duration, location) {
            $cordovaToast.show(message, duration, location).then(function(success) {
                console.log("The toast was shown");
            }, function (error) {
                console.log("The toast was not shown due to " + error);
            });
        }
        $scope.settings = {
            text: "logout",
            checked: true
        };

        $scope.logout = function () {
            Auth.logout();
            $state.go("login");
        }
    });
